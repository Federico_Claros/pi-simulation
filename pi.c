#include "pi.h"

int main(int argc, char **argv)
{
    double c_x = 0.0;
    double c_y = 0.0;
    double r_x = 0;
    double r_y = 0;
    double radius = 1.0;
    unsigned int I = 0;
    unsigned int ammount_tries = 0;
    unsigned int ammount_inside = 0;
    FILE * pf = NULL;

    printf("Ammount of arguments: %d", argc);

    ammount_tries = strtoul(argv[1], NULL, 10);

    srand(14);

    pf = fopen("points.txt", "w");

    if(NULL == pf)
      {
        printf("[ERR] El archivo 'points.txt' no se pudo crear!!\n");
        return -1;
      }

    while(I < ammount_tries)
      {
        r_x = -1 + 2 * ((double) rand() / (double) RAND_MAX);
        r_y = -1 + 2 * ((double) rand() / (double) RAND_MAX);

        save_point_to_txt(pf, r_x, r_y);

        if(is_inside_circle(c_x, c_y, radius, r_x, r_y))
          {
            ammount_inside++;
          }

        print_progress_bar((double) ((double) I/ (double) ammount_tries));
        I++;
      }

    fclose(pf);

    pf = fopen("results.txt", "w");

    if(NULL == pf)
      {
        printf("[ERR] El archivo 'results.txt' no se pudo crear!!\n");
        return -1;
      }

    fprintf(pf, "%f\n", get_error(ammount_tries));

    printf("\n[MSG] Error estimado: %f\n",
           get_error(ammount_tries));

    printf("[MSG] Cant. puntos estimados en el circulo %f\n",
           get_ammount_expected(ammount_tries));

    fprintf(pf, "%f\n", get_ammount_expected(ammount_tries));

    printf("[MSG] Con %d repeticiones.\n", ammount_tries);

    printf("[RES] %d poroto(s) cayeron dentro del circulo.\n",
           ammount_inside);

    fprintf(pf, "%d\n%d\n", ammount_inside, ammount_tries-ammount_inside);

    printf("[RES] %d poroto(s) cayeron fuera del circulo.\n",
           ammount_tries-ammount_inside);

    printf("[RES] Se estima que Pi = %f\n",
        (double)(4*((double)ammount_inside/(double)ammount_tries)));

    fprintf(pf, "%f\n",
        (double)(4*((double)ammount_inside/(double)ammount_tries)));

    fclose(pf);

    return 0;
}

unsigned int is_inside_circle(double c_x,
    double c_y, double radius,
    double p_x, double p_y)
{
    double result = 0;

    double delta_x = pow((p_x-c_x), 2);
    double delta_y = pow((p_y-c_y), 2);

    result = sqrt((delta_x + delta_y));

    if(result <= radius)
      {
        return 1;
      }
    return 0;
}

double get_error(unsigned int ammt)
{
    return sqrt(ammt);
}

double get_ammount_expected(unsigned int ammt)
{
    return 4 * (ammt);
}

void save_point_to_txt(FILE * file_pointer, double x, double y)
{
    fprintf(file_pointer, "(%f;%f)\n", x, y);
}

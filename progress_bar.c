//progress_bar.c
#include <stdio.h>

#define PBSTR "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

// Prompts programm progress in stdout.
// Given a fraction between 0 and 1.
void print_progress_bar(double fraction)
{
    int percentage = (int) (fraction * 100);
    int left_padding = (int) (fraction * PBWIDTH);
    int right_padding = PBWIDTH - left_padding;
    printf("\r%3d%% [%.*s%*s]",
           percentage, left_padding,
           PBSTR, right_padding, "");
    fflush(stdout);
    fflush(stdout);
}

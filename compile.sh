#!/bin/bash

# Compile progress_bar.c
gcc -Wall -Werror -Wextra -pedantic -o progress_bar.o -c -g progress_bar.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    echo "[ERR] compilation 'progress_bar.c': error code ${rVal}"
    exit "${rVal}"
fi

# Compile pi.c
gcc -Wall -Werror -Wextra -pedantic -o pi.o -c -g pi.c
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    echo "[ERR] compilation 'pi.c': error code ${rVal}"
    exit "${rVal}"
fi

# Link object files into executable named simulator_engine.
gcc -o simulator_engine pi.o progress_bar.o -lm
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    echo "[ERR] linking 'simulator_engine' failed! error code: ${rVal}"
    exit "${rVal}"
fi

# Compile file_reader.c
gcc -Wall -Werror -Wextra -pedantic -o file_reader.o -c -g file_reader.c
rVal=$?
if [ "${rVal}" -ne 0 ] ; then
    echo "[ERR] compilation 'file_reader' failed! error code: ${rVal}"
    exit "${rVal}"
fi

# Compile input_gui.c
gcc -Wall -Werror -Wextra -pedantic -o input_gui.o -c -g input_gui.c $(pkg-config --cflags gtk+-3.0)
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    echo "[ERR] compilation 'input_gui.c': error code ${rVal}"
    exit "${rVal}"
fi

# Compile main.c
gcc -Wall -Werror -Wextra -pedantic -o main.o -c -g main.c $(pkg-config --cflags gtk+-3.0)
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    echo "[ERR] compilation 'main.c': error code ${rVal}"
    exit "${rVal}"
fi

# Link object files into executable named pi_simulation.
gcc -Wall -Werror -Wextra -pedantic -o pi_simulation main.o input_gui.o file_reader.o $(pkg-config --libs gtk+-3.0)
rVal=$?
if [ "${rVal}" -ne 0 ]; then
    echo "[ERR] creation of executable named 'pi_simulation': error code ${rVal}"
    exit "${rVal}"
fi

# Remove object files from current directory.
rm -v *.o

echo "[MSG] Creation of executable named: 'simulator_engine'"
echo "[MSG] Creation of executable named: 'pi_simulation'"

# Pi simulation


The aim of this program is to approximate the Pi constant by generating an X number of random points over a circle.
By counting the points inside the circle, it is possible to approximate the value.
The more points inputted into the program, the better the approximation's output.


### Requirements


+ Python (> 3.0)
+ matplotlib
+ C compiler


>[!Note]
>The code was tested, compiled, and run on a GNU/Linux operating system.


### Contents:


+ compile.sh: a bash script that compiles all the C components.
+ check_python.sh: a bash script that checks if your PC has all Python modules.
+ file_reader: It's in charge of reading an arbitrary length from a file.
+ icon.png: the program's main icon.
+ input_gui: Generates a gtk window so you can do the simulation.
+ main: From which everything is organized and compiled.
+ pi: The heart of the simulation; it generates random points and accounts for them in order to approximate Pi. Generates two text files [^1].
+ plot_result.py: A small Python script that plots the randomly generated points over a circle, thus showing the simulation.
+ progress_bar: A nice TUI module that prompts a progress bar on the terminal.


### Usage:


1. Execute 'compile.sh' or compile the C programs manually.
2. If everything is okay, execute 'pi_simulation' on terminal[^2].


[^1]: "points.txt" and "results.txt"; the first one is the list of random points ready to be plotted; the latter is a summary of the simulation.
[^2]: by writing "./pi_simulation" on the terminal.


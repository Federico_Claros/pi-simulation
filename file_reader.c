#include "file_reader.h"

char* read_line(FILE *file) {
    int buffer_size = INITIAL_BUFFER_SIZE;
    char *buffer = malloc(buffer_size * sizeof(char));

    if (NULL == buffer)
      {
        fprintf(stderr, "Memory allocation error\n");
        return NULL;
      }

    int position = 0;
    int c;

    while (1)
      {
        c = fgetc(file);

        // Check for end of file or newline
        if (EOF == c || c == '\n')
          {
            buffer[position] = '\0';
            break;
          }

        buffer[position] = c;
        position++;

        // If the buffer is full, extend it
        if (position >= buffer_size)
          {
            buffer_size *= 2;
            buffer = realloc(buffer, buffer_size * sizeof(char));
            if (NULL == buffer)
              {
                fprintf(stderr, "Memory allocation error\n");
                return NULL;
              }
          }
      }

    // If we reached the end of file without reading any characters
    if (position == 0 && EOF == c)
      {
        free(buffer);
        return NULL;
      }

    return buffer;
}

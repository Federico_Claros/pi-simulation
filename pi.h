//pi.h
#ifndef _PI_ENGINE_H
#define _PI_ENGINE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "progress_bar.h"

// determines whenether or not a point is inside a circle.
// Returns 0, if point is not in circle. 1, otherwise.
unsigned int is_inside_circle(double c_x,
    double c_y, double radius,
    double p_x, double p_y);

double get_ammount_expected(unsigned int ammt);

double get_error(unsigned int ammt);

void save_point_to_txt(FILE *, double, double);

#endif

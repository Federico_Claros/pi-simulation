//file_reader.h
#ifndef _FILE_READER_H
#define _FILE_READER_H

#define INITIAL_BUFFER_SIZE 10

#include <stdio.h>
#include <stdlib.h>

// Reads a line with arbitrary length.
char* read_line(FILE *);

#endif

#!/bin/bash

# Check if python3 is available
if command -v python3 &>/dev/null; then
    python_cmd="python3"
elif command -v python &>/dev/null; then
    python_cmd="python"
else
    echo "Python is not found in PATH. Please install Python."
    exit 1
fi

echo "Python command found: $python_cmd"

# Check Python version
python_version=$($python_cmd -c 'import sys; print(sys.version_info[:])')
echo "Python version: $python_version"

# Check if matplotlib is installed
$python_cmd -c 'import matplotlib' >/dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "matplotlib is installed."
else
    echo "matplotlib is NOT installed."
fi

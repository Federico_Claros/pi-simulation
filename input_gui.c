//input_gui.c
#include "input_gui.h"
#include "file_reader.h"

void
call_program_simulation_engine(unsigned int ammount_tries)
{
	char command[100];
	sprintf(command, "./simulator_engine %u", ammount_tries);
	system(command);
}

void
call_program_plot_results(GtkDialog *dialog, gint response_id, gpointer user_data)
{
	(void) user_data;

	if (response_id == GTK_RESPONSE_OK)
	  {
        system("python3 plot_results.py");
      }

    gtk_widget_destroy(GTK_WIDGET(dialog)); // Ensure the dialog is destroyed
}

void
dialog_init(GtkWidget ** dialog, GtkWindow *parent, GtkDialogFlags flags,
                 GtkMessageType type, GtkButtonsType buttons,
                 const gchar* msg)
{
	*dialog = gtk_message_dialog_new(
		parent,
		flags,
		type,
		buttons,
		"%s", msg
	);

	gtk_window_set_icon_from_file(GTK_WINDOW(*dialog), "icon.png", NULL);
}



void
prepare_results(char **resultados, FILE *fp)
{
	char *buffer;

	if(NULL == *resultados)
	{
		*resultados = (char *) malloc(1024 * sizeof(char));
		if(NULL == resultados)
		  {
			show_error_dialog("No se pudo reservar memoria para los resultados!");
			return;
		  }
		(*resultados)[0] = '\0';
	}

	strcat(*resultados, "Error estimado: ");

	buffer = read_line(fp);

	if(NULL != buffer)
	  {
		strcat(*resultados, buffer);
		free(buffer);
	  }

	strcat(*resultados, "\nPuntos esperado en circulo: ");

	buffer = read_line(fp);

	if(NULL != buffer)
	  {
		strcat(*resultados, buffer);
		free(buffer);
	  }

	strcat(*resultados, "\nCant. puntos dentro del circulo: ");

	buffer = read_line(fp);

	if(NULL != buffer)
	  {
		strcat(*resultados, buffer);
		free(buffer);
	  }

	strcat(*resultados, "\nCant. puntos fuera del circulo: ");

	buffer = read_line(fp);

	if(NULL != buffer)
	  {
		strcat(*resultados, buffer);
		free(buffer);
	  }

	strcat(*resultados, "\nPi estimado: ");

	buffer = read_line(fp);

	if(NULL != buffer)
	  {
		strcat(*resultados, buffer);
		free(buffer);
	  }
	strcat(*resultados, "\n");
}

void
show_ask_4_continue(const gchar *continue_msg)
{
	GtkWidget *dialog = NULL;
	GtkWidget *label = NULL;
	FILE *fp = NULL;
	char *resultados = (char *) malloc(1024 * sizeof(char));

	if (!resultados)
	  {
		show_error_dialog("No se pudo reservar memoria para los resultados!");
		return;
	  }

	resultados[0] = '\0';

	if(NULL == (fp = fopen("results.txt", "r")))
	  {
		show_error_dialog("No se encontro el archivo: 'results.txt'");
		free(resultados);
		return;
	  }

	prepare_results(&resultados, fp);

	fclose(fp);

	label_init(&label, resultados);

	if(!label)
	  {
		show_error_dialog("Fallo la inicializacion del label!");
		free(resultados);
		return;
	  }

	dialog_init(&dialog, NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_QUESTION, GTK_BUTTONS_OK_CANCEL, continue_msg);

	if (!dialog)
	{
        show_error_dialog("Fallo la inicializacion del dialog!");
        gtk_widget_destroy(label);
        free(resultados);
        return;
    }

	gtk_container_add(
		GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(dialog))),
		label);

	g_signal_connect(dialog, "response", G_CALLBACK(call_program_plot_results), NULL);

	gtk_widget_show_all(dialog);
	gtk_dialog_run(GTK_DIALOG(dialog));
	free(resultados);
}

void
show_error_dialog(const gchar *error_msg)
{
	GtkWidget *dialog;
	
	dialog_init(&dialog, NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, error_msg);
	
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

void
button_clicked (GtkWidget *widget, gpointer data)
{
	GtkWidget *entry = GTK_WIDGET(data);
	const gchar *text = gtk_entry_get_text(GTK_ENTRY(entry));
	int ammount_tries = atoi(text);
	
	if(ammount_tries < 0)
	  {
		show_error_dialog("Debe ser un valor NO negativo!!");
		return;
	  }
	
	gtk_widget_set_sensitive(widget, FALSE);
	
	call_program_simulation_engine(ammount_tries);
	show_ask_4_continue("Quiere ver el grafico resultante? (puede tardar mucho)");
	
	gtk_widget_set_sensitive(widget, TRUE);
}

void
window_init(GtkWidget **window, char *title, unsigned int width, unsigned int height)
{
	*window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	gtk_window_set_title(GTK_WINDOW(*window), title);
	gtk_window_set_default_size(GTK_WINDOW(*window), width, height);
	gtk_window_set_icon_from_file(GTK_WINDOW(*window), "icon.png", NULL);
	
	g_signal_connect(*window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
}

void
vertical_box_init(GtkWidget **vbox, GtkWidget *window)
{
	*vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	
	gtk_container_add(GTK_CONTAINER(window), *vbox);
}

void
button_init(GtkWidget **button, GtkWidget *entry)
{
	*button = gtk_button_new_with_label("Run!");
	
	g_signal_connect(*button, "clicked", G_CALLBACK(button_clicked), entry);
}

void
label_init(GtkWidget **label, char *text)
{
	*label = gtk_label_new(text);
}

void
add_vertical_box(GtkWidget **vertical_box, GtkWidget *widget)
{
	gtk_box_pack_start(GTK_BOX(*vertical_box), widget, TRUE, TRUE, 5);
}

import matplotlib.pyplot as plt

l_x = []
l_y = []

plt.axes()
circle = plt.Circle((0,0),1, fc='white', ec='black')
plt.gca().add_patch(circle)
plt.axis('scaled')

with open('points.txt', 'r') as f:
    for line in f:
        ch_index = line.find(';')
        ch_fin = line.find(')')
        x = line[1:ch_index]
        y = line[ch_index+1:ch_fin]
        l_x.append(float(x))
        l_y.append(float(y))

plt.plot(l_x, l_y,'b,')
plt.show();

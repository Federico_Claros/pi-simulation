//main.c
#include "input_gui.h"

int main(int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *vertical_box;
	GtkWidget *entry;
	GtkWidget *button;
	GtkWidget *label;

	gtk_init(&argc, &argv);

	window_init(&window, "Pi simulation", 300, 150);

	vertical_box_init(&vertical_box, window);

	label_init(&label, "Ingrese la cantidad de repeticiones");

	entry = gtk_entry_new();

	button_init(&button, entry);

	add_vertical_box(&vertical_box, label);
	add_vertical_box(&vertical_box, entry);
	add_vertical_box(&vertical_box, button);

	gtk_widget_show_all(window);

	gtk_main();

	return 0;
}

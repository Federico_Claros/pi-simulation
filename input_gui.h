//input_gui.h
#ifndef _INPUT_GUI_H
#define _INPUT_GUI_H
#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

void call_program_simulation_engine(unsigned int ammount_tries);

void call_program_plot_results(GtkDialog *dialog, gint response_id, gpointer user_data);

void show_error_dialog(const gchar *error_msg);

void button_clicked (GtkWidget *widget, gpointer data);

void window_init(GtkWidget **window, char *title, unsigned int width, unsigned int height);

void vertical_box_init(GtkWidget **vbox, GtkWidget *window);

void button_init(GtkWidget **button, GtkWidget *entry);

void label_init(GtkWidget **label, char *text);

void dialog_init(GtkWidget ** dialog, GtkWindow *parent, GtkDialogFlags flags,
                 GtkMessageType type, GtkButtonsType buttons,
                 const gchar* msg);

void prepare_results(char **results, FILE *fp);

void add_vertical_box(GtkWidget **vertical_box, GtkWidget *widget);

void show_ask_4_continue(const gchar *continue_msg);

#endif
